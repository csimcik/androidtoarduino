# Android To Arduino Test #


### Test app for working with the [Physicaloid Library](https://github.com/ksksue/PhysicaloidLibrary) to communicate serially with Arduino and update Arduino firmware via usb  ###

* If connecting to rgb led; red lead to pin 11, green lead to pin 10, and blue lead to pin 9 on *UNO* 

* **Should** open app when connecting any arduino *UNO* to phone (possible a board may have the other vendor id)

* hit *Load* and choose a hex file 

* Toggle connection (bottom right) to open/close channel to Arduino

